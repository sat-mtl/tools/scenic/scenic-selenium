#!/bin/bash
# Script to start video recording, run a test and stop recording.
#   Usage: test-and-record.sh PYTEST_FILE_PATH ALLOW_TEST_FAILURE
#   Return value
#     Always 0 if ALLOW_TEST_FAILURE is set to "true"
#     pytest return code if ALLOW_TEST_FAILURE is set to "false"
#   Examples
#     test-and-record.sh path/to/known-good-pytest-test-file.py false
#     test-and-record.sh path/to/known-failing-pytest-test-file.py true

start_recording() {
  # get pytest file path from first argument
  local PYTEST_FILE_PATH="$1"

  # build video recording directory path and create it
  local TEST_DIR_PATH="recording/$(dirname $PYTEST_FILE_PATH)"
  mkdir -p "$TEST_DIR_PATH"

  # build video filename and path
  local VIDEO_FILENAME="$(basename --suffix=.py $PYTEST_FILE_PATH).mkv"
  local VIDEO_FILE_PATH="$TEST_DIR_PATH/$VIDEO_FILENAME"

  ffmpeg -loglevel error \
         -nostats \
         -y \
         -f x11grab \
         -video_size 1920x1080 \
         -i :0 \
         -codec:v libx264 \
         -crf 52 \
         -preset ultrafast \
         -r 30 \
         "$VIDEO_FILE_PATH" &
}

stop_recording() {
  pkill ffmpeg
}

test_and_record() {
  local PYTEST_FILE_PATH="$1"
  local ALLOW_TEST_FAILURE="${2:-false}"

  echo "****** Start $PYTEST_FILE_PATH ******"
  start_recording $PYTEST_FILE_PATH
  pipenv run pytest $PYTEST_FILE_PATH
  # store test result return code so we can stop ffmpeg without loosing test result
  local TEST_RETURN_CORE=$?
  stop_recording
  # override pytest return code with 0 to allow pipeline to continue even if test fail
  if [ $ALLOW_TEST_FAILURE == "true" ]; then exit 0; else exit $TEST_RETURN_CORE; fi
}

test_and_record $1 $2