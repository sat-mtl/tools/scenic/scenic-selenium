# Scenic-Selenium

Scenic-Selenium is a repository of automated tests written and performed with the Selenium IDE Chrome extension (there exists, also, a Firefox version).  The repository of tests can be run using a Chrome installation, with the Selenium IDE extension installed, by pointing the browser at any IP address that is hosting a Scenic instance.  The aim of this repository is to automate UI and display testing tasks such that QAs can shift their focus to more intensive functional test coverage.

**UPDATE:** Scenic-Selenium is currently being totally rewritten in Selenium Webdriver(Python).
 Please see the [Webdriver Readme](../../doc/README.md) for the latest and for how to install and use the webdriver suite.

## Getting Started

This document will detail the installation and process for running these tests

### Prerequisites

1) Install Chrome; it is recommended that a tester maintain a 'clean' version of Chrome such that it contains no browser extensions or personalizations other than Selenium IDE browser extension in order to avoid false positives or negatives caused by the behaviour of said extensions.

2) Install the Selenium IDE Chrome Extension
<https://chrome.google.com/webstore/detail/selenium-ide/mooikfkahbdckldjjndioackbalphokd>

### Installing(the repo)

Once Chrome and Selenium IDE are installed, installation is as simple as cloning this repository, and opening the test directory while inside Selenium IDE

```
git clone git@gitlab.com:sat-mtl/telepresence/scenic-selenium.git
cd scenic-selenium
git checkout develop

```

That's it!

## Running the tests

### Prepare Scenic for testing

1) Launch any Scenic instance
2) Navigate to the "Paramètres" panel in the left sidebar
3) Remove the "Authentification" option for remote access

### Launch and run Selenium IDE

1) Launch Chrome
2) Click on the Selenium IDE Extension
3) Click on "Open an existing project"
4) Navigate into the newly cloned `scenic-selenium` directory
Each .side file contains a suite of tests relating to an area of Scenic's UI; select one in order to be presented with a list of automated test cases.
5) In Selenium IDE's navigation bar, add `http://` followed by the IP address of the Scenic instance you are testing.
6) Select a test case from the left sidebar, and press the play icon in the tool bar in order to run the test.  Alternatively, press the "Play All" button in order to run all of the tests in a single test suite at once.

## Known Issues

* Selenium can playback a test faster than a user can execute it, which is great, but sometimes Scenic will not keep up.  A best practice is to set the playback speed to about three quarters speed.  This can be automatically set in the future.
* Tests have been designed to test the desired behaviour of Scenic; this means that some tests currently fail by design because the current behaviour of a component does not match the desired behaviour.  This is normal.
* Some tests need to log a user into SIP in order to run; sometimes it takes several attempts to do so.  A test that needs to do so will fail if the log-in fails.  This will be fixed in future versions.
* Some tests that test NDI features may fail if there are other NDI streams available on the local network.  It is a good idea to make sure there are no other NDI streams available before running these tests.

## Documentation by Test Suite

* [Menu Test Cases](menu-testnotes.md)
* [Matrix Test Cases](matrix-testnotes.md)
* [Preview, Timelapse, and Thumbnail Test Cases](previewicon-testnotes.md)
* [Scene switching test cases](scenes-testnotes.md)
* [SIP Call Test Cases WIP](sip-testnotes.md)
