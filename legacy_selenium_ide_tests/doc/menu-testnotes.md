# Menu Test Cases

## MenuTest Case List Notes

1. Verifies the text of all menu headings, sub-headings and items in the Sources menu
2. Verifies the text of all menu headings, sub-headings and items in the Destinations menu
3. Verifies the text of all menu headings, sub-headings and items in the Compression menu
4. Adds one of each physical input from the source menu, and checks to see if the item still exists in the menu.
6. Ensures the existence of the language switcher, switches language to English, and tests the names of all menu items and sub-items against the expected.
7. Adds all sources and destinations and then adds each filter subsequently to make sure that the right items are present.  Afterwards it removes all of the filters to ensure that the order of the sources and destinations matches the original order.
11. a) Verifies that the names of sources are user friendly after being added to a scene b) does the same for Destinations.
