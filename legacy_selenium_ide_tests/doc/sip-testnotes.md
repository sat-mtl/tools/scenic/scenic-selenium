# WIP: SIP Test Cases

These tests are a work in progess; some of them don't work yet, but most of them do.  When loading these test cases in Selenium IDE, if a test is marked with an asterisk, it means it does not quite function as intended yet(though the test itself will run and flag results).

These test cases test the SIP login and calling functions; as such, it is necessary to have two Scenic stations ready for these tests.  Some of these tests will operate both stations at the same time so it is therefore necessary to make sure that the SAT1 and SAT 2 logins are available and not being used for anything else; the QASIP logins are not useable for some of these test cases because some of them involve testing the Do Not Disturb functionality.

## Known issues, concerns, and etc

* As mentioned above, one needs both the SAT and SAT2 logins to be available, and two stations available
* The two stations in these tests are currently set to be Chaos (default) and Scenicbox B.  In order to change this one must update the default target, as per normal, but also do a find and replace for the Scenicbox B IP address with whatever one's desired Item Under Test is.
* If a user logs out during an active SIP call, that Scenic instance will be unable to initiate any further SIP calls until the instance has been restarted.  While this test suite takes that into consideration during it's init and exit phases, it is important to note this fact if a test is interrupted.

## SIP Test Case Notes

1. Tests logging in first by clicking 'Login' and then by using the 'Enter' key.  The test looks for error prompts and labels for the 'Disconnect,' 'Add a Contact' and 'ONLINE' in both languages; it also tests that users can log out without error messages.
2. Tests if, on login, active users appear as online and with a green bar in a users contact list(by checking the border-left-color of the .contact-info).  This test will stop if login fails
3. Pending
4. Pending
5. This test logs in and adds a contact to the matrix while checking for error flags
6. This test logs in, adds a contact, and removes it while checking for error flags
7. This test logs in, adds a contact, adds a source, and attempts to complete a SIP Call.  It verifies that a call has been completed by checking to see if the 'hangup' option exists in the contact menu.
11. This test logs into station A and sets their status to Do Not Disturb; it then switches to Station B and attempts to call Station A with an audio test signal 3 times and checks whether or not the menu item switches to the 'hangup' action.  It then logs out and initalises the scenes of both stations.
12. This test logs into Station A, sets their status to Do Not Disturb, adds the SAT contact to their Exception list, switches to Station B, logs in, adds Station A as a SIP destination, adds an Audio Test Signal, and attempts to call Station A with the test signal. It then checks to see that there is a 'hang up' action available.  Additionally it switches to Station A to make sure the sent stream exists in the matrix there.

### Additional Test Cases

* DUAL STATION INITIALIZATION/REINITIALIZATION TEMPLATE is a template that automates both Logging in and out of Station A and B after hanging up an active SIP call before initializing the scenes as per normal practice.  This is because the process for init and exit is complex enough that copying it everytime becomes convoluted.
* the ORIGINAL Ensure that a user can sign into SIP(1) test case is included though it did not work properly because it contains some actions and css selectors that are not easy to copy/paste from other tests and/or automate via the context menu.
