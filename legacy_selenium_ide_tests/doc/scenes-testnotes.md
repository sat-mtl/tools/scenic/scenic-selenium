# Scene Change UI Automated Test Cases

These tests are written for the scene change and scrollbar UI functionality

## Scene Change UI Test Case notes

1. Loads Scenic, checks to see only the default scene exists and that it has a green active label
2. Loads scenic, checks to see the default scene doesn't have a delete button, adds a new scene and makes sure that the new scene does have a delete button.
3. Checks to make sure that the Add scene button exists and that it works.
4. Adds a Scene, clicks the remove button, verifies the modal text and options that appears, confirms the delete operation and verifies that the scene no longer exists.
5. Renames the default scene and checks to make sure it was renamed
6. Adds two scenes, deletes Scene 1, adds a third and verifies it is called Scene 3, the id is Scene 3 and that Scene 1 no longer exists.  It then renames Scene 2 and adds a 4th Scene and checks to make sure it is called Scene 4 and the id is Scene 4 and that Scene 1 still does not exist.
7. Instantiates 2 sources, adds a new scene, sets it to active and ensures that it is active and that the default scene is no longer active.
8. Adds two scenes, instantiates and powers on sources, sets a scene as active and attempts to delete it while verifying the modal options and the text of the warning that appears.
9. The test case that would have existed here (SCENIC-3464) is covered in the Matrix automated test cases extensively and exhaustively.
10. This test is a prototype as the scroll buttons are not currently implemented; hence there is no english version as the test will eventually need to be re-written.  For now, it adds a number of scenes and then scrolls back to the delete the first one in order to verify that scrolling is possible when the number of scenes exceeds the total.
