# Matrix Test Cases

The matrix test automation tests the behaviour of the connection matrix and, even when run manually, greatly speeds up regression testing.  Note that there are several tests in this suite that require there to be no previously running NDI streams in order to ensure an accurate test result.

## Matrix Test Case List Notes

1. This test instantiates all sources and destinations and checks to make sure the matrix does not appear before sources turn on.
2. This test logs into SIP instantiates all sources, destinations, and SIP contacts and then powers them on.  It then checks to make sure that each individual cell in the matrix exists, but does not distinguish between valid and invalid sources.  Additionally, it checks to make sure that the NDI input is appropriately adding connections according to which type of sources are attached to the stream.
13. This test instantiates all sources and destinations to make sure they appear at the end of the matrix when added.  It then ensures that, when either a source or destination is deleted and re-added, it appears at the end of the matrix.  Note that, in the current behaviour of Scenic, compression destinations and sources always appear the end of the matrix, and this is considered the expected behaviour in the scope of this test.
14. This test ensures that refreshing the interface does not change the order of sources or destinations by instantiating all of each, registering the order, and then refreshing the browser and re-checking.
15. This test ensures that changing the language does not change the order of sources or destinations by instantiating all of each, registering the order, and then changing the language and re-checking.
16. This test ensures that changing scenes does not change the order of sources or destinations by instantiating all of each, registering the order, and adding two new scenes, switching to them, and re-checking.
17. This test checks to see that Audio sources can only be connected to valid Audio or Network destinations by instantiating all Audio sources, and then instantiating all possible destinations.  It then attempts, first, to make all invalid connections and to check that a .disconnect .icon does not appear.  Afterwards, it checks to make sure all valid connections are possible by making the connection, checking to make sure a .disconnect .icon appears, and then removing the connection so the next connection can be tested.
18. This test ensures that only one Audio source can be connected to an XR18 Output by instantiating two audio signals, connecting one to an XR18 Output, and then connecting the other and looking to see that only .connection:nth-child(1) exists and not .connection:nth-child(2).  Afterwards it includes a sanity check to make sure that connections are removed when the user attempts to do so; this is an artifact of an earlier iteration and can be removed in the future.
19. This test performs the same test as 18 but with inactive scenes and for armed states using the .selected class
20. This test instantiates all Video sources and an XR18 Output and attempts to make sure the Video sources can not be assigned to the XR18 output by attempting the connection and then looking for a .disconnect > .icon
21. This test performs the same test as 20 but with Control sources.
22. This test instantiates all destinations, all Video sources, and attempts to ensure the sources can only be assigned to valid destinations by first attempting invalid connections and then looking for a .disconnect > .icon and then checking valid connections to see if a .disconnect > .icon appears and then removing the connection in order to check the next connection.  It, additionally, makes sure that adding a source to an H264 encoder doesn't nullify the rules for valid and invalid connections (ie, it checks to make sure you cannot add a Video source to an invalid destination by way of encoding).  This is a particularly large test.
23. This test ensures all physical Video sources have a raw and an encoded stream available by checking the information tab for each source.
24. This test ensures only one Video source can be assigned to a video monitor by by instantiating two Video signals, connecting one to a Video Monitor, and then connecting the other and looking to see that only .connection:nth-child(1) exists and not .connection:nth-child(2)
25. This test performs the same test as 24 but for inactive scenes and armed states.
26. This test ensures a user cannot assign an audio source to a Video monitor by attempting the connection and looking for a .disconnect > .icon
27. This test is the same as 26 but for Control sources
28. This test performs the same tests as 17 and 22 but for Control sources
29. This test ensures only one Midi input can be assigned to a Midi output by assigning first one and then the other and making sure only a .connection:nth-child(1) exists and not .connection:nth-child(2).
30. This test checks to see if you can assign an Audio source to a Midi output by attempting the connection and looking for a .disconnect > .icon
31. This test performs the same test as 30 but with Video sources
32. This test performs the same test as 30 but with a Video source attached to an H264 Encoder and an Audio source attached to a Decoder; it uses the .connection:nth-child(n) class structure to make sure only 1 connection exists.
33. This test performs the same test as 30 but with an OSC input
34. This test ensures only one OSC input can be assigned to an OSC output by assigning first one and then the other and making sure only a .connection:nth-child(1) exists and not .connection:nth-child(2).
35. This test checks to see if you can assign an Audio source to an OSC output by attempting the connection and looking for a .disconnect > .icon
36. This test performs the same test as 35 but with Video sources
37. This test performs the same test as 35 but with encoded sources(as per test 32)
38. This test performs the same test as 35 but with a Midi input
39. This test ensures it is possible to connect any Audio or Video source to an A/V Decoder by instantiating the sources and decoder and then attempting the connection, looking for a .disconnect > .icon and then removing the connection for the next test.
40. This test ensures that only one Video or Audio source can be assigned to an A/V Decoder by attempting the first connection, and then attempting the second and ensuring that the .disconnect >.icon only appears for that .source:nth-child and not the other
41. Performs the same test as 40 but for inactive scenes and armed states(using .selected)
42. Ensures that only one Video source can be connected to an H264 encoder by attempting to make first one connection and the the other and looking for a .source:nth-child(1) .connection:nth-child(2) .disconnect > .icon to see if there is more than one.
43. Ensures a user cannot assign an encoded stream to an H264 encoder by attempting to make those connections and looking for a .disconnect > .icon
44. Performs the same test as 43 for Audio sources and includes NDI audio sources
45. Performs the same test as 44 but for Control sources
46. This test checks to make sure it is possible to only assign one Audio source to an NDI output by instantiating two audio sources, adding one to the NDI output, ensuring a .disconnect > .icon exists and then making the second connection and checking to make sure a .connect > .icon (an empty, valid, cell) exists.
47. Performs the same test as 46 but with inactive scenes and armed states
48. Performs the same test as 46 but with Video sources
49. Same as 48 with inactive scenes and armed states.
50. Performs the same test as 46 but using an RTMP Output instead of NDI
51. Same as 50 for inactive scenes and armed states
52. Same as 50 with Video sources
53. Same as 52 for inactive scenes and armed states.
54. Ensures that the connection matrix for an NDI input is appropriate to the sources assigned to the corresponding NDI output by instantiating both and each of an Audio and Video source, and assigning each to the NDI output while checking the cells exist and verifying the title of the information panel for each.
55. Ensures an NDI input can be assigned to an NDI output by instantiating both, instantiating an Audio and  Video source, assigning them to the NDI output, and then removing the original Audio and Video sources and making sure the connection exists when a user attempts to assign the appropriate NDI streams back to the NDI output.
56. Does not exist.  
57. Ensures that an unencoded Video stream can not be sent to a SIP contact by logging into SIP, instantiating all video sources and attempting to assign the unencoded stream to the SIP contact and looking for a .disconnect > .icon
58. Ensures that all other sources can be sent to SIP contacts by attempting the connection, looking for the .disconnect > .icon and then removing the connection for the next test.
