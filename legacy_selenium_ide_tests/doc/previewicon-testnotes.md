# Preview, Timelapse and Thumbnail Icon Test Cases

## Preview, Timelapse and Thumbnail Icon Test notes

2. Instantiates each of the video inputs and checks for the correct thumbnail icon before deleting it and checking the next.  Also includes the test for the H264 encoder stream.
3. Instantiates each of the audio inputs and checks for the correct thumbnail icon before deleting it and checking the next.
4. Instantiates each of the data inputs and checks for the correct thumbnail icon before deleting it and checking the next.
5. Instantiates an NDI output, an XR18 Input, a Video Test Signal, connects them too the NDI output, enables the NDI output, instantiates an NDI input, deletes the test signal and XR18 input, and checks for the correct icon and preview image.
