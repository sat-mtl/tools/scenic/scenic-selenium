from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
import time


def test_video_test_pattern_height_slider(scenic_instance_1) -> None:
    try:

        scenic_instance_1.create_quiddity_from_menu('Sources', 'Audio', 'Test Signal')
        scenic_instance_1.power_on_SDI_source()
        # Find the test pattern label and click it to open the properties label
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.SourceHead[data-source="sdiInput10"]').click()
        time.sleep(2)

        # Find the Capture sub-menu
        scenic_instance_1.driver.find_element(
            By.CSS_SELECTOR, '#PropertyInspectorDrawer .GroupTitle[data-menu="Capture"]').click()
        time.sleep(2)
        # TODO!!! This test will no longer work until the resolution field is set to 'custom' - in fact this may render the whole test obsolete
        # Find and read the value of the height Input Field
        # TODO find Resolution dropdown
        # TODO click 'Custom'
        sdi1_height_slider_selector = '.PropertyField[data-quiddity="sdiInput10"][data-property="Capture/height"]'
        height_value_t0 = scenic_instance_1.driver.find_element(
            By.CSS_SELECTOR, sdi1_height_slider_selector + ' .InputNumberField').get_attribute('value')
        print(str(height_value_t0))

        # Find the increment button for the height property, click it, and store the resulting value
        scenic_instance_1.driver.find_element(
            By.CSS_SELECTOR, sdi1_height_slider_selector + ' .InputButtonIncrement').click()
        time.sleep(2)

        height_value_t1 = scenic_instance_1.driver.find_element(
            By.CSS_SELECTOR, sdi1_height_slider_selector + ' .InputNumberField').get_attribute('value')
        print(str(height_value_t1))
        # TODO!!! /end

        # Check to see if the height value at t1 is greater than the value at origin.
        assert height_value_t1 > height_value_t0, 'The height slider is not working properly.'
        print('The height slider is working as expected')

        # We have to change focus back to the Scenic main view now so that we can interact with the quidditie
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#MatrixPage').click()
        # Power on the test source
        scenic_instance_1.power_on_SDI_source()

        # Verify the appropriate properties are now disabled (the started_toggle and pattern_dropdown should still be operable)
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.SourceHead[data-source="sdiInput10"]').click()

        # Find the Capture sub-menu
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR,
                                              '#PropertyInspectorDrawer .GroupTitle[data-menu="Capture"]').click()
        time.sleep(2)

        # Find the increment button for the height property, click it, and store the resulting value
        scenic_instance_1.driver.find_element(
            By.CSS_SELECTOR, sdi1_height_slider_selector + ' .InputButtonIncrement').click()
        time.sleep(2)
        height_value_t2 = scenic_instance_1.driver.find_element(
            By.CSS_SELECTOR, sdi1_height_slider_selector + ' .InputNumberField').get_attribute('value')

        print(str(height_value_t2))

        # Check to see if the height value at t3 is still equal to the value at t2
        assert height_value_t2 == height_value_t1, 'The height slider is still enabled when the source is powered on'
        print('The height slider is disabled, and this is good because it is supposed to be disabled.')

    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'
