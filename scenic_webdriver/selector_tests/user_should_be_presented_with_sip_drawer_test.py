import time
import os
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By

def test_sip_drawer_elements(scenic_instance_1):
    # This tests for the presence of the status bar elements and sections
    SERVER_ADDRESS = os.environ["SERVER_ADDRESS"]
    USER1_SIP_SOURCE_PORT = os.environ["USER1_SIP_SOURCE_PORT"]
    USER1_NAME = os.environ["USER1_NAME"]
    USER1_PASSWORD = os.environ["USER1_PASSWORD"]

    try:
        # Open the SipDrawersMenu and test its elements
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.SipDrawersMenu').click()
        scenic_instance_1.assert_element_presence('.SipDrawersMenu .icon-close') # Checks to make sure the 'close' state is appled to the button
        scenic_instance_1.assert_element_presence('.ResetButton')
        scenic_instance_1.assert_element_presence('.LoginButton')
        scenic_instance_1.assert_element_presence('.InputTextField[name="sipServer"]')
        scenic_instance_1.assert_element_presence('.InputTextField[name="sipUser"]')
        scenic_instance_1.assert_element_presence('.InputTextField[name="sipPassword"]')

        # Login to SIP and check for its elements
        scenic_instance_1.login_to_sip(SERVER_ADDRESS, USER1_SIP_SOURCE_PORT, USER1_NAME, USER1_PASSWORD)
        scenic_instance_1.assert_element_presence('#SipContactDrawer')
        scenic_instance_1.assert_element_presence('#SessionPartnerSection')
        scenic_instance_1.assert_element_presence('#SessionContactSection')
        scenic_instance_1.assert_element_presence('.LogoutButton')
        scenic_instance_1.assert_element_presence('.SessionContact')
        scenic_instance_1.assert_element_presence('.SipContactButton .icon-plus')
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.SipContactButton').click()
        scenic_instance_1.assert_element_presence('.SipContactButton .icon-minus')

    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'
