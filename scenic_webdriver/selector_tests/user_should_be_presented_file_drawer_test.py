from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By

def test_file_drawer_elements(scenic_instance_1):
    # This tests for the presence of the status bar elements and sections
    try:
        # Open the OpenFileMenu and test its elements
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.OpenSessionMenu').click()
        scenic_instance_1.assert_element_presence('.OpenSessionMenu .icon-close') # Checks to make sure the 'close' state is appled to the button
        scenic_instance_1.assert_element_presence('.UploadButton')
        scenic_instance_1.assert_element_presence('.DownloadButton')
        scenic_instance_1.assert_element_presence('.OpenFileButton')

        # Open the SaveAsMenu and test its elements
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.SaveAsSessionMenu').click()
        scenic_instance_1.assert_element_presence('.SaveAsSessionMenu .icon-close')
        scenic_instance_1.assert_element_presence('.NewFileButton')
        scenic_instance_1.assert_element_presence('.SaveAsButton')

        # Open the DeleteFileMenu and test its elements
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.DeleteMenuButton').click()
        scenic_instance_1.assert_element_presence('.DeleteMenuButton .icon-close')
        scenic_instance_1.assert_element_presence('.ResetButton')
        scenic_instance_1.assert_element_presence('.DeleteButton')

    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'
