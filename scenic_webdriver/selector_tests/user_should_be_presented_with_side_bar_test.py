from selenium.common.exceptions import NoSuchElementException

def test_side_bar_elements(scenic_instance_1):
    # This tests for the presence of sidebar menu elements but not the menus therein
    try:

        scenic_instance_1.assert_element_presence('.PropertyInspectorMenu')
        scenic_instance_1.assert_element_presence('.SipDrawersMenu')
        scenic_instance_1.assert_element_presence('.NewSessionMenu')
        scenic_instance_1.assert_element_presence('.OpenSessionMenu')
        scenic_instance_1.assert_element_presence('.SaveSessionMenu')
        scenic_instance_1.assert_element_presence('.SaveAsSessionMenu')
        scenic_instance_1.assert_element_presence('.DeleteMenuButton')

    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'
