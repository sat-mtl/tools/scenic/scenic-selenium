from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By

def test_page_menu_elements(scenic_instance_1):
    # This tests for the presence of page menu and the tab elements (but not the pages themselves)
    # TODO  none of these tabs have specific selectors so this test uses xpaths, so this is not in the spirit of the scope of this test suite but it is included for the sake of completion
    try:

        #assert scenic_instance_1.driver.find_element(By.XPATH, '//div[contains(text(), "scenes")]'), 'The Scenes page selector has changed'
        assert scenic_instance_1.driver.find_element(By.XPATH, '//div[contains(text(), "matrix")]'), 'The Scenes page selector has changed'
        #assert scenic_instance_1.driver.find_element(By.XPATH, '//div[contains(text(), "live")]'), 'The Scenes page selector has changed'
        assert scenic_instance_1.driver.find_element(By.XPATH, '//div[contains(text(), "settings")]'), 'The Scenes page selector has changed'
        assert scenic_instance_1.driver.find_element(By.XPATH, '//div[contains(text(), "help")]'), 'The Scenes page selector has changed'

    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'
