from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
import time


def test_locked_ndi_outputs(scenic_instance_1) -> None:

    try:

        # Instantiate an audio and a video source
        scenic_instance_1.create_quiddity_from_menu('Sources', 'Audio', 'Test Signal')
        scenic_instance_1.create_quiddity_from_menu('Sources', 'Video', 'SDI 1')

        # Instantiate an NDI output TODO does not work right now because the destinations menu appears to be uninteractable
        scenic_instance_1.create_quiddity_from_menu('Destinations', 'Network', 'NDI® Output')

        # Power on Audio and Video Source
        scenic_instance_1.activate_source('audioTestInput1')
        scenic_instance_1.activate_source('sdiInput11')

        # Ensure there are no locked connections or sources
        assert scenic_instance_1.search_for_locked_sources() == 0, 'There are already locked sources'
        assert scenic_instance_1.search_for_locked_connections() == 0, 'There are already locked connections'

        # Connect powered on sources to NDI Output
        scenic_instance_1.connect_quiddities('audioTestInput1', 'ndiOutput1')
        scenic_instance_1.connect_quiddities('sdiInput11', 'ndiOutput1')

        # Activate NDI Output
        scenic_instance_1.activate_ndi_output()

        # Look for locked connections and sources
        assert scenic_instance_1.search_for_locked_sources() == 2, 'The expected number of locked sources were not found'
        assert scenic_instance_1.search_for_locked_connections() == 2, 'The expected number of locked connections were not found'  #TODO this currently always fails because locked connections for certain quiddities don't register

    except NoSuchElementException as e:
        scenic_instance_1.return_to_main_page()
        assert False, f'UI element could not be found. Error: {e.msg}'
