from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException


def test_locked_connection_seeker(scenic_instance_1) -> None:

    try:

        number_of_locked_sources = scenic_instance_1.search_for_locked_sources()
        number_of_locked_connections = scenic_instance_1.search_for_locked_connections()

    except NoSuchElementException as e:
        scenic_instance_1.return_to_main_page()
        assert False, f'UI element could not be found. Error: {e.msg}'
