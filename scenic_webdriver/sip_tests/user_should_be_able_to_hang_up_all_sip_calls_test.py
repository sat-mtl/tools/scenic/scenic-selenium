from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
import time
import os

def test_hang_up_all(scenic_instance_1, scenic_instance_2, scenic_instance_3) -> None:
    try:
        SERVER_ADDRESS = os.environ["SERVER_ADDRESS"]

        USER1_SIP_SOURCE_PORT = os.environ["USER1_SIP_SOURCE_PORT"]
        USER1_NAME = os.environ["USER1_NAME"]
        USER1_PASSWORD = os.environ["USER1_PASSWORD"]

        USER2_SIP_SOURCE_PORT = os.environ["USER2_SIP_SOURCE_PORT"]
        USER2_NAME = os.environ["USER2_NAME"]
        USER2_PASSWORD = os.environ["USER2_PASSWORD"]

        USER3_SIP_SOURCE_PORT = os.environ["USER3_SIP_SOURCE_PORT"]
        USER3_NAME = os.environ["USER3_NAME"]
        USER3_PASSWORD = os.environ["USER3_PASSWORD"]

        # Find window handle for scenic instance 1
        scenic_instance_1_window_handle = scenic_instance_1.driver.current_window_handle
        print("Scenic Instance 1 window handle: " + scenic_instance_1_window_handle)

        # Login to SIP on station 1
        scenic_instance_1.login_to_sip(SERVER_ADDRESS, USER1_SIP_SOURCE_PORT, USER1_NAME, USER1_PASSWORD)

        # Add User2 as a SIP contact in User1's matrix
        print("I'm gonna click the User 2 contact add button now")
        scenic_instance_1.driver.find_element(
            By.CSS_SELECTOR, f'.SessionContact[data-contact="{USER2_NAME}@{SERVER_ADDRESS}"] .SipContactButton').click()
        time.sleep(2)

        # Add User3 as a SIP contact in User1's matrix
        print("I'm gonna click the User 3 contact add button now")
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR,
                                              f'.SessionContact[data-contact="{USER3_NAME}@{SERVER_ADDRESS}"] .SipContactButton').click()
        time.sleep(2)

        # Close the SIP drawer so it's not in the way later
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR,
                                              '.SipDrawersMenu').click()

        # Initialise scenic station 2 and connect to switcher
        time.sleep(3)
        scenic_instance_2_window_handle = scenic_instance_2.driver.current_window_handle
        print("Scenic Instance 2 Window Handle: " + scenic_instance_2_window_handle)

        # Login to SIP on station 2
        scenic_instance_2.login_to_sip(SERVER_ADDRESS, USER2_SIP_SOURCE_PORT, USER2_NAME, USER2_PASSWORD)

        # Add User1 as a SIP contact in User2's matrix
        print("[User 2]: I'm gonna click the User 1 contact name now!")
        scenic_instance_2.driver.find_element(By.CSS_SELECTOR,
                                              f'.SessionContact[data-contact="{USER1_NAME}@{SERVER_ADDRESS}"] .SipContactButton').click()
        time.sleep(2)
        # Close the SIP drawer, again, so it's not in the way later
        scenic_instance_2.driver.find_element(By.CSS_SELECTOR,
                                              '.SipDrawersMenu').click()

        # Initialise scenic station 3 and connect to switcher
        time.sleep(3)
        scenic_instance_3_window_handle = scenic_instance_3.driver.current_window_handle
        print("Scenic Instance 3 Window Handle: " + scenic_instance_3_window_handle)

        # Login to SIP on station 3
        scenic_instance_3.login_to_sip(SERVER_ADDRESS, USER3_SIP_SOURCE_PORT, USER3_NAME, USER3_PASSWORD)

        # Add User1 as a SIP contact in User3's matrix
        print("[User 3]: I'm gonna click the User 1 contact name now!")
        scenic_instance_3.driver.find_element(By.CSS_SELECTOR,
                                              f'.SessionContact[data-contact="{USER1_NAME}@{SERVER_ADDRESS}"] .SipContactButton').click()
        time.sleep(2)
        # Close the SIP drawer, again, so it's not in the way later
        scenic_instance_3.driver.find_element(By.CSS_SELECTOR,
                                              '.SipDrawersMenu').click()

        # Return to User1's station
        print('I will try to switch back to Scenic 1 now')
        scenic_instance_1.driver.switch_to.window(scenic_instance_1_window_handle)
        time.sleep(2)

        # Open the Sources menu on Scenic 1, find the audio submenu, click it, and instantiate a audio test signal
        scenic_instance_1.create_quiddity_from_menu('Sources', 'Audio', 'Test Signal')

        # Find the power on switch and click it
        scenic_instance_1.activate_source('audioTestInput1')

        # Connect the stream to User 2
        print("Assigning source to User 2")
        scenic_instance_1.connect_to_sip_destination('audioTestInput1', 'sip', USER2_NAME)
        time.sleep(2)

        # Connect the stream to User 3
        print("Assigning source to User 3")
        scenic_instance_1.connect_to_sip_destination('audioTestInput1', 'sip', USER3_NAME)
        time.sleep(2)

        # Open the SIP Drawer
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR,
                                              ".SipDrawersMenu").click()

        # Attempt to complete the SIP call to User 2
        print("trying to find the call button for User 2!")
        scenic_instance_1.driver.find_element(
            By.CSS_SELECTOR, f'.SessionPartner[data-contact="{USER2_NAME}@{SERVER_ADDRESS}"] .SipContactButton').click()

        # Attempt to complete the SIP call to User 3
        print("trying to find the call button for User 3!")
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR,
                                              f'.SessionPartner[data-contact="{USER3_NAME}@{SERVER_ADDRESS}"] .SipContactButton').click()
        time.sleep(3)

        # ENSURE THE SIP CALLS WORKED
        # Switch to User2's Station
        print('I will try to switch back to Scenic 2 now')
        scenic_instance_2.driver.switch_to.window(scenic_instance_2_window_handle)

        # Ensure that the audio test source has appeared on the matrix
        scenic_instance_2.assert_sip_source_presence('audioTestInput1')

        # Switch to User3's Station
        print('I will try to switch back to Scenic 3 now')
        scenic_instance_3.driver.switch_to.window(scenic_instance_3_window_handle)

        # Ensure that the audio test source has appeared on the matrix
        scenic_instance_3.driver.implicitly_wait(5)
        scenic_instance_3.assert_sip_source_presence('audioTestInput1')

        # Switch to Scenic 1 Window
        print('I will try to switch back to Scenic 1 now')
        scenic_instance_1.driver.switch_to.window(scenic_instance_1_window_handle)
        time.sleep(2)

        # Ensure the Hang Up All Button appeared
        hang_up_all_button = scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.HangupAllButton')
        assert hang_up_all_button is not None, 'The Hang Up All Button did not appear, but the SIP calls appeared to complete'

        # Attempt to click the Hang Up All Button
        hang_up_all_button.click()
        scenic_instance_1.driver.implicitly_wait(5)  # It takes some time for the interface to adjust

        # Click the Matrix page to return focus to it and to close the SIP drawer
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#MatrixPage').click()

        # Search for any locked connection boxes; if there are any, the hangup all button didn't work.
        assert scenic_instance_1.search_for_locked_sources() == 0, "There are still locked connections, so the Hang Up All Button didn't work"

        # Dummy check to make sure that the Hang Up All Button is no longer displayed
        hang_up_all_button = scenic_instance_1.driver.find_elements(By.CSS_SELECTOR, '.HangupAllButton')
        assert len(hang_up_all_button) == 0, "The Hang Up All Button is still displayed, but we could not detect any locked connection boxes, so it probably worked but is still showing."

    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'
