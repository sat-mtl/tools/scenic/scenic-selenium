from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
import time


def test_scene_deletion(scenic_instance_1) -> None:
    try:
        # This time.sleep ensures that the driver doesn't attempt to click the scene adder before the initialize method from scenic_driver is completely finished.
        time.sleep(5)
        # Click the scene adder; this line will also assert the presence of the scene-adder due to the nature of Selenium
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.AddSceneButton').click()

        # Attempt to click the close button for the new scene
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.DeleteSceneButton').click()

        # Determine if the Delete Scene Modal is showing
        delete_scene_modal = scenic_instance_1.driver.find_elements(By.CSS_SELECTOR, '#DeleteSceneModal')
        assert delete_scene_modal, 'The user was not asked for confirmation of scene deletion'

        # Click the confirmation button
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.DeleteButton').click()

        # Check to see if the scene was deleted by searching for scene close buttons again
        assert not scenic_instance_1.driver.find_elements(
            By.CSS_SELECTOR, '.DeleteSceneButton'), 'The scene was not deleted.'

    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'
