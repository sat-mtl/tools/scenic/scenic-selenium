from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support.color import Color
import time


def test_possibility_of_active_scene_deletion(scenic_instance_1) -> None:
    try:
        # This time.sleep ensures that the driver doesn't attempt to click the scene adder before the initialize method from scenic_driver is completely finished.
        time.sleep(5)
        # Click the scene adder; this line will also assert the presence of the scene-adder due to the nature of Selenium
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.AddSceneButton').click()

        # Activate the new Scene
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#SceneControlPanel .Checkbox-active').click()

        # Ensure the Scene is active by checking to see if the selected tab now has a green active Checkbox-tiny
        colour_of_active_indicator = Color.from_string(scenic_instance_1.driver.find_element(
            By.CSS_SELECTOR, '.navtab-tab-selected .navtab-tab-addon .Checkbox-tiny .styledCheckbox-0-2-221').value_of_css_property('background-color')).hex

        assert colour_of_active_indicator == '#38c566'

        # Ensure the close button was removed
        assert not scenic_instance_1.driver.find_elements(By.CSS_SELECTOR, '.DeleteSceneButton')

    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'
