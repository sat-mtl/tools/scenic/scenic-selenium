from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
import time
import uuid

def test_NewFileModal(scenic_instance_1) -> None:
    TEST_FILE_NAME_1 = 'SeleniumTestFile' + str(uuid.uuid4())

    try:
        # Click on the 'Save As' icon
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.SaveAsSessionMenu[type="button"]').click()

        # Click on the 'New File' Button
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.NewFileButton').click()

        # Assert the presence of the NewFileModal
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#NewFileModal'), 'The NewFileModal did not appear'

        # Try to click the 'Create' button without entering a file name and ensure it does not work
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.ConfirmButton').click()

        # Assert the presence of the cancel button
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.CancelButton'), 'The NewFileModal does not have a cancel button'

        # Assert the presence of the text field input
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.NewFileNameInput'), 'The text field for entering a file name did not appear on the NewFileModal'

        # Cancel the save function to ensure it can be cancelled
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.CancelButton').click()

        # Use the abstraction to ensure proper save function; first close the file drawer
        scenic_instance_1.close_active_drawer()
        scenic_instance_1.save_session_file(TEST_FILE_NAME_1)

        # Delete the test file to avoid save file pollution
        scenic_instance_1.delete_session_file(TEST_FILE_NAME_1)

    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'
