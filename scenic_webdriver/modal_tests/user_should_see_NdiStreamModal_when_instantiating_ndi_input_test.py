from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
import time

def test_NdiStreamModal(scenic_instance_1) -> None:

    try:
        # Case 1: no existing NDI streams
        # Instantiate an NDI Input
        scenic_instance_1.create_quiddity_from_menu('Sources', 'Network', 'NDI® Input')
        time.sleep(3) # Allow the NdiStreamModal time to load

        # Assert the presence of the NdiStreamModal
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#NdiStreamModal'), 'The NdiStreamModal did not appear when instantiating an NDI input with no active NDI streams available'
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.CancelButton'), 'The NdiStreamModal appeared but did not contain a cancel button'

        # Try to click the Ajouter button; this will also confirm its presence, but, it should be disabled.
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.ConfirmButton').click()

        # Assert the modal is still visible
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#NdiStreamModal'), 'Clicking the confirm button while there were no active NDI streams available closed the modal; it should not have.'

        # Cancel the NDI input instantiation, and assert no NDI input was added
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.CancelButton').click()
        number_of_ndi_inputs_in_matrix = scenic_instance_1.driver.find_elements(By.CSS_SELECTOR, '.SourceHead[data-nickname="ndiInput1"]')
        assert len(number_of_ndi_inputs_in_matrix) == 0, 'The NDI input was added despite the user cancelling'

        # Case 2: existing NDI Stream
        scenic_instance_1.create_quiddity_from_menu('Destinations', 'Network', 'NDI® Output') #TODO doesn't work right now because network group is non-interactable

        # Open the property panel
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#DestinationHead').click() #TODO assumes only one destination because destinations don't have specific selectors yet

        # Activate the NDI output
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.SwitchLabel').click()

        # Instantiate an NDI Input
        scenic_instance_1.create_quiddity_from_menu('Sources', 'Network', 'NDI® Input')
        time.sleep(3) # Allow the NdiStreamModal time to load

        # Assert the presence of the NdiStreamModal
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#NdiStreamModal'), 'The NdiStreamModal did not appear when instantiating an NDI input with no active NDI streams available'
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.CancelButton'), 'The NdiStreamModal appeared but did not contain a cancel button'

        # Try to click the Ajouter button; this will also confirm its presence, but, it should be disabled if no stream is selected
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.ConfirmButton').click()

        # Assert the modal is still visible
        assert scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#NdiStreamModal'), 'Clicking the confirm button with no active NDI stream selected closed the modal; it should not have.'

        # Select an active NDI stream
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.SelectButton').click()
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.SelectOption').click()

        # Click confirm
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.ConfirmButton').click()

        # Assert that an NDI input was instantiated
        number_of_ndi_inputs_in_matrix = scenic_instance_1.driver.find_elements(By.CSS_SELECTOR,
                                                                                '.SourceHead[data-nickname="ndiInput1"]')
        assert len(number_of_ndi_inputs_in_matrix) == 1, 'The NDI input instantiation did not work as expected'

    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'
