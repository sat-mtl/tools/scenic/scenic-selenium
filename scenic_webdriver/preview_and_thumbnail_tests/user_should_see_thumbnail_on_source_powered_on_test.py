from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
import time


def test_thumbnail(scenic_instance_1) -> None:

    try:

        scenic_instance_1.create_quiddity_from_menu('Sources', 'Video', 'SDI 1')
        time.sleep(2)
        scenic_instance_1.activate_source('sdiInput11')

        # Find the thumbnail at t1 and get its img src value TODO --- this selector is not particularly semantic
        thumbnail_img = scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '#common-sources > div > div > div > span > div > div > div > div > div > img')
        thumbnail_state1 = thumbnail_img.get_attribute('src')
        time.sleep(2)

        # Find the thumbnail at t2 and get its img src value
        thumbnail_state2 = thumbnail_img.get_attribute('src')

        # Since the src value is dynamically updated as frames change, if thumbnail_state1 is not equal to thumbnail_state2, we can infer the thumbnail is working
        assert thumbnail_state1 != thumbnail_state2, 'The thumbnail is not updating correctly'
        print("The thumbnail is updating correctly")

    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'
