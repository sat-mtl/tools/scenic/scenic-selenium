from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
import time
import uuid


def test_saved_session(scenic_instance_1) -> None:
    try:
        TEST_FILE_NAME_1 = 'SeleniumTestFile' + str(uuid.uuid4())

        scenic_instance_1.create_quiddity_from_menu('Sources', 'Video', 'SDI 1')
        scenic_instance_1.activate_source('sdiInput11')

        # Click on the 'Save As' icon
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.SaveAsSessionMenu[type="button"]').click()

        # Determine how many saved files already exist
        previously_saved_file_list = scenic_instance_1.driver.find_elements(By.CSS_SELECTOR,
                                                                            '#FileSavingDrawer .FileEntry')
        # Click on the 'New File' Button
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.NewFileButton').click()

        # Find the 'File name' field and enter a filename

        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.NewFileNameInput').send_keys(TEST_FILE_NAME_1)

        # Find and click the create button
        scenic_instance_1.driver.find_element(By.CSS_SELECTOR, '.ConfirmButton').click()

        # Determine how many saved files now exist
        current_saved_file_list = scenic_instance_1.driver.find_elements(By.CSS_SELECTOR,
                                                                         '#FileSavingDrawer .FileEntry')

        # If the list of files has grown since the start of this test, saving worked.
        assert len(current_saved_file_list) > len(previously_saved_file_list), 'Saving did not work'
        print('The saved file exists!')

        # Find and delete the test file
        scenic_instance_1.delete_session_file(TEST_FILE_NAME_1)

    except NoSuchElementException as e:
        assert False, f'UI element could not be found. Error: {e.msg}'
