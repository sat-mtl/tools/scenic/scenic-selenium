from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
import time
import ipaddress

# Define number of attempts for SIP login
MAX_ATTEMPTS = 5


class ScenicDriver:
  def __init__(self, scenic_url: str) -> None:

    options = webdriver.ChromeOptions()
    options.add_argument("--disable-dev-shm-usage")
    options.add_argument("--no-sandbox")
    self.driver = webdriver.Chrome(options=options)
    self.scenic_url = scenic_url
    self.driver.maximize_window()

    self.driver.get(self.scenic_url)

    self.driver.implicitly_wait(2)

  def set_switcher_instance(self, switcher_host: str = '', switcher_port: str = '') -> None:
    print('setting switcher instance...')
    url = switcher_host if switcher_host else '0.0.0.0'
    port = switcher_port if switcher_port else '8000'
    # If scenic-selenium is running on local host, the switcher config modal does not appear, so we try to find the modal and if we fail we move on
    try:
      # Find the switcher remote instance modal's Host Url field and connect button
      host_url_field = self.driver.find_elements(By.CSS_SELECTOR,
                                                 '#SwitcherConnectionModal .UrlInput')

      if len(host_url_field) > 0:
        host_url_field = self.driver.find_element(By.CSS_SELECTOR,
                                                  '#SwitcherConnectionModal .UrlInput')
        port_field = self.driver.find_element(By.CSS_SELECTOR, '.PortInput')
        switcher_connect_button = self.driver.find_element(By.CSS_SELECTOR,
                                                           '#SwitcherConnectionModal .ConnectButton')
        # Clear the host url field, enter the base URL as the switcher URL and click connect
        host_url_field.clear()
        host_url_field.send_keys(url)
        port_field.clear()
        port_field.send_keys(port)
        switcher_connect_button.click()
        print('...done')

    except NoSuchElementException:
      print('Scenic-Selenium is running on Localhost and so no modal appears.  All is ok!')

  def reload_scenic(self) -> None:
    # This checks to see if the invalid user configuration modal appears and clicks Reload if it does
    try:
      self.driver.find_element(By.CSS_SELECTOR, '.ReloadButton').click()
    except NoSuchElementException:
      print('The Invalid User Configuration Modal did not appear.')

  def initialize_scenic(self) -> None:
    # Click the 'new' button and initalise scenic
    print('initializing scenic...')
    self.driver.find_element(By.CSS_SELECTOR, '.NewSessionMenu .icon-new-file').click()
    # Click the confirmation button in the modal
    self.driver.find_element(By.CSS_SELECTOR, '.ConfirmButton').click()
    print('...done')

  def assert_element_presence(self, element):
      '''
        Searches for the presence of an element by creating a list of all elements and then asserts that the length of this list is not zero
        This is preferable to simply attempting to interact with the element because not all elements are interactive and because we may want to expand
        on this functionality later.
      '''
      list_of_elements = self.driver.find_elements(By.CSS_SELECTOR, f'{element}')
      assert len(list_of_elements) != 0, f'It appears that the selector for {element} has changed, or this element no longer exists'

  def assert_sip_source_presence(self, sipSource):
    '''
      Searches for the sip source header that appears on a scenic instance, and then checks its shmdata path to make sure
      it is non-empty; this could be refined to look for particular shmdata path structures if that is deemed necessary
    '''

    transmitted_test_source = self.driver.find_element(By.CSS_SELECTOR, f'.SourceHead[data-nickname="{sipSource}"]')
    assert transmitted_test_source, 'SIP transmission of test source failed.'
    print('SIP transmission of test source successful!')
    time.sleep(2)
    self.driver.find_element(By.CSS_SELECTOR, f'.SourceHead[data-nickname="{sipSource}"]').click()
    shmdata_path = self.driver.find_element(By.CSS_SELECTOR,
                                                         f'.PropertyField[data-nickname="{sipSource}"][data-property="shmdata-path"] .InputTextField').get_attribute('value')
    assert shmdata_path, 'The shmdata path is empty so the call may have failed'

  def create_quiddity_from_menu(self, menu, group, item) -> None:
    self.driver.find_element(By.CSS_SELECTOR,
                             f'.Menu .SelectButton[data-menu="{menu}"]').click()

    # Adapt the function in case the item has no group
    if not group is None:
      self.driver.find_element(By.CSS_SELECTOR,
                               f'.Menu .GroupTitle[data-menu="{group}"]').click()

    self.driver.find_element(By.CSS_SELECTOR,
                             f'.Menu .Item[data-menu="{item}"]').click()

  def open_video_preview_window(self, source, stream) -> None:
    self.driver.find_element(By.CSS_SELECTOR,
                             f'.SourceShmdata[data-shmdata="/tmp/switcher_{source}_1_{stream}"]').click()

  def activate_source(self, item):
    self.driver.find_element(By.CSS_SELECTOR,
                             f'.SourceStarter[data-nickname="{item}"]').click()

  def open_source_properties(self, source) -> None:
    self.driver.find_element(By.CSS_SELECTOR, f'.SourceHead[data-nickname="{source}"]').click()

  def open_property_sub_menu(self, menu) -> None:
    self.driver.find_element(By.CSS_SELECTOR, f'#PropertyInspectorDrawer .GroupTitle[data-menu="{menu}"]').click()

  def connect_quiddities(self, source, destination) -> None:
    self.driver.find_element(By.CSS_SELECTOR,
                             f'.ConnectionBox[data-nickname="{source}_{destination}"]').click()

  def connect_to_sip_destination(self, source, destination, user_name) -> None:
    self.driver.find_element(By.CSS_SELECTOR,
                             f'.ConnectionBox[data-nickname="{source}_{destination}_{user_name}"]').click()

  def activate_ndi_output(self):
    # Open the property drawer for the NDI output and click it; this currently assumes there is only one destination, and it must because the destination head is generic right now (no custom selectors)
    self.driver.find_element(By.CSS_SELECTOR, '#DestinationHead').click()
    self.driver.find_element(By.CSS_SELECTOR, '.SwitchButton').click()
    time.sleep(2)

  def generate_fake_rtmp_creds(self):
    # Fills in an rtmp output creation modal with fake credentials.  This function also implies the existence of a generate_real_rtmp_creds.  alas, there is none.
    self.driver.find_element(By.CSS_SELECTOR, '.UrlInput').send_keys('thiscouldliterallybeanything')
    self.driver.find_element(By.CSS_SELECTOR, '.StreamKeyInput').send_keys(
      'becausethereisnovalidationonthesefields')
    time.sleep(2)
    self.driver.find_element(By.CSS_SELECTOR, '.ConfirmButton').click()

  def activate_rtmp_output(self):
    # Open the property drawer for the RTMP output and click it; this currently assumes there is only one destination
    self.driver.find_element(By.CSS_SELECTOR, '#DestinationHead').click()
    self.driver.find_element(By.CSS_SELECTOR, '.SwitchButton').click()
    time.sleep(2)

  def close_active_drawer(self):
    # Looks for the active MenuButton and clicks it.
    self.driver.find_element(By.CSS_SELECTOR, '.MenuButton-active').click()

  def save_session_file(self, test_file_name: str) -> None:
    # Click on the 'Save As' icon
    self.driver.find_element(By.CSS_SELECTOR, '.SaveAsSessionMenu[type="button"]').click()
    # Click on the 'New File' Button
    self.driver.find_element(By.CSS_SELECTOR, '.NewFileButton').click()
    # Find the 'File name' field and enter a filename
    print(test_file_name)
    self.driver.find_element(By.CSS_SELECTOR, '.NewFileNameInput').send_keys(test_file_name)
    # Find and click the create button
    self.driver.find_element(By.CSS_SELECTOR, '.ConfirmButton').click()

  def load_session_file(self, test_file_name: str, load_file=True) -> None:
    # Check to see if the #FileLoadingDrawer is displayed
    file_loading_drawer = self.driver.find_element(By.CSS_SELECTOR, '#FileLoadingDrawer')
    if not file_loading_drawer.is_displayed():
      self.driver.find_element(By.CSS_SELECTOR, '.OpenSessionMenu').click()

    self.driver.find_element(By.CSS_SELECTOR,
                             f'#FileLoadingDrawer .RowEntry[data-entry="{test_file_name}"]').click()
    self.driver.find_element(By.CSS_SELECTOR, '.OpenFileButton').click()
    if load_file:
      # Confirm changes when prompted
      self.driver.find_element(By.CSS_SELECTOR, '.ConfirmButton').click()
      time.sleep(2)

  def download_session_file(self, test_file_name: str) -> None:
    # Open the file drawer and download the test file and wait for it to download
    self.driver.find_element(By.CSS_SELECTOR, '.OpenSessionMenu').click()
    self.driver.find_element(By.CSS_SELECTOR,
                             f'#FileLoadingDrawer .RowEntry[data-entry="{test_file_name}"]').click()
    self.driver.find_element(By.CSS_SELECTOR, '#FileLoadingDrawer .DownloadButton').click()
    # This time.sleep is IMPORTANT - without it, the file will be given a temporary name by Chrome, for some reason
    time.sleep(2)

  def delete_session_file(self, test_file_name: str, delete_file=True) -> None:
    # Check to see if the Delete File Drawer is already open or not:
    file_deletion_drawer = self.driver.find_element(By.CSS_SELECTOR, '#FileDeletionDrawer')
    if not file_deletion_drawer.is_displayed():
      self.driver.find_element(By.CSS_SELECTOR, '.DeleteMenuButton').click()

    # Find the saved file and click it to highlight it
    self.driver.find_element(By.CSS_SELECTOR,
                             f'#FileDeletionDrawer .RowEntry[data-entry="{test_file_name}"]').click()
    if delete_file:
      did_the_TrashCurrentSessionModal_appear = self.driver.find_elements(By.CSS_SELECTOR, '#TrashCurrentSessionModal')

      if len(did_the_TrashCurrentSessionModal_appear) == 1:
        self.driver.find_element(By.CSS_SELECTOR, '.ConfirmButton').click()

      # Find the delete button and click it
      self.driver.find_element(By.CSS_SELECTOR, '.DeleteButton').click()

  def login_to_sip(self, server_address: str, sip_source_port: str, user_name: str, sip_password: str) -> None:
    print('Running login_to_sip')
    # Open the SIP menu if it's not already open
    is_the_sip_menu_open = self.driver.find_elements(By.CSS_SELECTOR, '.SipDrawersMenu .icon-close')
    if len(is_the_sip_menu_open) == 0:
      self.driver.find_element(By.CSS_SELECTOR,
                              '.SipDrawersMenu').click()
      print('Opening SIP drawer')

    # Find SIP credential fields and send them the appropriate credentials
    self.driver.find_element(By.CSS_SELECTOR,
                             '.InputTextField[name="sipServer"]').send_keys(server_address)
    print('Found address input field')
    self.driver.find_element(By.CSS_SELECTOR,
                             '.InputTextField[name="sipPort"]').clear()
    self.driver.find_element(By.CSS_SELECTOR,
                             '.InputTextField[name="sipPort"]').send_keys(sip_source_port)
    print('Found SIP source port input field')
    self.driver.find_element(By.CSS_SELECTOR,
                             '.InputTextField[name="sipUser"]').send_keys(user_name)
    print('Found user input field')
    self.driver.find_element(By.CSS_SELECTOR,
                             '.InputTextField[name="sipPassword"]').send_keys(sip_password)
    print('Found password input field')

    # Attempt to login, and allow the scenic instance to load
    login_button = self.driver.find_element(By.CSS_SELECTOR, '.LoginButton')
    login_attempt_counter = 1

    while login_attempt_counter <= MAX_ATTEMPTS:
      login_button.click()
      time.sleep(5)

      if not login_button.is_displayed():
        print("login attempt successful!")
        break

      print("login attempt " + str(login_attempt_counter) + " unsuccessful")
      login_attempt_counter += 1

    assert not login_button.is_displayed(), 'Login failed.'

  def return_to_main_page(self) -> None:
    # This function is essential for the function of the page test suite
    # Ensure the user is back on the main page

    #self.driver.find_element(By.XPATH, '//div[contains(text(), "scenes")]').click()
    self.driver.find_element(By.XPATH, '//div[contains(text(), "matrix")]').click()

  def select_property_dropdown_button(self, quiddity_nickname: str, property_id: str):

    return self.driver.find_element(By.CSS_SELECTOR,
                                    f'.PropertyField[data-nickname="{quiddity_nickname}"][data-property="{property_id}"] .SelectButton')

  def search_for_locked_sources(self):
    '''
    Searches all the locked sources in the matrix displayed in a Scenic instance.
      Returns:
        All sources head elements that are locked.
    '''
    return len(self.driver.find_elements(By.CSS_SELECTOR, '.SourceLock'))

  def search_for_locked_connections(self):
    '''
    Searches all the locked connections in the matrix displayed in a Scenic instance.
      Returns:
        All connection elements that are locked.
    '''
    return len(self.driver.find_elements(By.CSS_SELECTOR, '.LockedBox'))

  def tear_down_scenic(self) -> None:
    # Check if the user is logged into SIP and log them out if they are
    print('checking to see if user is logged into SIP...')

    # Check to see if the logout button is displayed
    print('Looking for the Logout button')
    logout_button = self.driver.find_element(By.CSS_SELECTOR, '.LogoutButton')

    if logout_button.is_displayed():
      print('User was logged into SIP; attempting to logout now.')
      self.driver.find_element(By.CSS_SELECTOR, '.LogoutButton').click()

    else:
      print('Attempting to open SIP menu')
      self.driver.find_element(By.CSS_SELECTOR,
                               '.SipDrawersMenu').click()

      # Check to see if there is a logout button now; if there isn't, it means the user never logged into sip so they don't have to log out
      logout_button = self.driver.find_element(By.CSS_SELECTOR, '.LogoutButton')
      if logout_button.is_displayed():
        logout_button.click()

    # Click New button to empty the scene
    print('exiting scenic...')
    self.driver.find_element(By.CSS_SELECTOR, '.icon-new-file').click()
    # Click reset confirmation modal button
    self.driver.find_element(By.CSS_SELECTOR, '.ConfirmButton').click()
    print('...done')
    self.driver.quit()
