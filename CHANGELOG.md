Release Notes
===================

scenic-selenium 1.1.0 (2023-05-25)
---------------------------------

 * 🐛 Update test of the URLs on the Scenic help page ([!95](https://gitlab.com/sat-mtl/tools/scenic/scenic-selenium/-/merge_requests/95))
 * ✅ Adapt scenic-selenium for 4.1 ([!94](https://gitlab.com/sat-mtl/tools/scenic/scenic-selenium/-/merge_requests/94))
 * 👷 Ameliorate pipeline ([!91](https://gitlab.com/sat-mtl/tools/scenic/scenic-selenium/-/merge_requests/91))
 * 👷 Add basic CI pipeline ([!90](https://gitlab.com/sat-mtl/tools/scenic/scenic-selenium/-/merge_requests/90))
 * ♻ Refactor all sip tests so they look for shmdata path ([!89](https://gitlab.com/sat-mtl/tools/scenic/scenic-selenium/-/merge_requests/89))
 * ✨ Introduce element seeker feature and basic selector presence tests ([!87](https://gitlab.com/sat-mtl/tools/scenic/scenic-selenium/-/merge_requests/87))
 * 🌱 Add scenic-selenium CI contacts ([!88](https://gitlab.com/sat-mtl/tools/scenic/scenic-selenium/-/merge_requests/88))
 * 📝 Correct minor errors in documentation regarding pipenv installation ([!85](https://gitlab.com/sat-mtl/tools/scenic/scenic-selenium/-/merge_requests/85))
 * 🐛 Fix scene-ui tests broken due to changed selectors ([!84](https://gitlab.com/sat-mtl/tools/scenic/scenic-selenium/-/merge_requests/84))
 * ♻ Refactor various elements of quiddity control ([!83](https://gitlab.com/sat-mtl/tools/scenic/scenic-selenium/-/merge_requests/83))
 * ♻ Refactor quiddity connection and sip destination connection ([!82](https://gitlab.com/sat-mtl/tools/scenic/scenic-selenium/-/merge_requests/82))
 * ✨ Feat/automate modal test cases ([!81](https://gitlab.com/sat-mtl/telepresence/scenic-selenium/-/merge_requests/81))
 * ♻ Refactor source activation and add a test for source activation ([!80](https://gitlab.com/sat-mtl/telepresence/scenic-selenium/-/merge_requests/80))
 * 🐛 Change outdated selectors for updated ones ([!79](https://gitlab.com/sat-mtl/telepresence/scenic-selenium/-/merge_requests/79))
 * ♻ Refactor quiddity creation ([!78](https://gitlab.com/sat-mtl/telepresence/scenic-selenium/-/merge_requests/78))
 * ✨Feat/locked connection tests ([!77](https://gitlab.com/sat-mtl/telepresence/scenic-selenium/-/merge_requests/77))
 * 🐛 Fix broken preview and thumbnail tests due to img src attributes ([!76](https://gitlab.com/sat-mtl/telepresence/scenic-selenium/-/merge_requests/76))
 * 🐛 Fix disabled property tests ([!75](https://gitlab.com/sat-mtl/telepresence/scenic-selenium/-/merge_requests/75))
 * 🗑 Deprecate custom resolution tests ([!74](https://gitlab.com/sat-mtl/telepresence/scenic-selenium/-/merge_requests/74))
 * 🚨 Format everything ([!72](https://gitlab.com/sat-mtl/telepresence/scenic-selenium/-/merge_requests/72))
 * 🔧 Update dev tools and issue templates ([!71](https://gitlab.com/sat-mtl/telepresence/scenic-selenium/-/merge_requests/71))

scenic-selenium 1.0.0 (2022-05-11)
----------------------------------

  * ✨ Add selenium automated tests for menus
  * ✨ Add Known issues and Matrix TC documentation
  * 🐛 Fix several bugs with matrix test automation
  * ♻ Refactor Menu tests and fix some matrix bugs
  * ♻ Reorganize and revise repo documentation and file structure
  * ✨ Add React Preview and Thumbnail automated tests
  * 🐛 Add English test versions and fix several bugs
  * 🐛 Add English language Matrix test automation
  * ✨ Add automated tests for Scene changing UI
  * 🚧 Add prototype SIP call automated tests
  * ✨ Add tests for dev server log in and contact seed
  * ✨ Add automation for SIP2 login and contact seed tests
  * ✨ Add SIP login prototype and directory
  * ✨ Add video preview test prototype and reorganize dirs
  * ✨ Add prototype for source thumbnail tests
  * ✨ Add lifecycle functions prototype and simple boot test
  * ✨ Open SIP test repo and add login prototype
  * 🐛 Fix bug occuring when user runs scenic on localhost
  * ✨ Refactor code so that it uses dotenv for environmental variables
  * 📝 Add webdriver readme and update IDE readme
  * ✏ Fix dotenv variables and some selectors
  * ✨ Add a Pipfile
  * ♻ Restructure scenic-selenium more logically
  * ♻ Make scenic-selenium work with pytest
  * ✨ Add working unidirectional SIP call test
  * ✨ Add teardown function to scenic_driver
  * 📝 Update README, make filenames consistent, update .gitignore
  * ✨ Add working bilateral SIP test
  * 🚨 Improve error catching and session tests
  * 🐛 Fix teardown function and some minor mistakes and typos elsewhere
  * 🐛 Fix height slider test by switching focus to Matrix Page
  * ✨ Add test for width slider manipulation
  * ✨ Add video properties test for the resolution dropdown
  * ✨ Add video property test for framerate dropdown
  * ✨ Add load session test and begin scenic_driver refactor
  * ✨ Add test for Hang Up All function and support for 3 Scenic instances
  * ✨ Add page test suite to scenic-selenium
  * 🎨 Move test source creation and session file manipulation to scenic_driver
  * ♻ Refactor SIP tests to work with f strings for users and server address
  * 🐛 Add a function to return to the Scenes page when finishing page tests
  * 🐛 Fix test suite so it is compatible with Scenic Web and update docs
  * ✨ Change SIP tests to work with Audio Test Signals
  * 🐛 Fix issue regarding scenic_driver url logic
  * 🐛 Fix assertion logic when evaluating whether a list is empty in scenic_selenium
  * ✨ Create scene_ui_tests directory and add default scene test
  * 🧑‍ Add Feature Request and Bug Resolution Request templates
  * ♻ Make Scenic-Selenium work on localhost
  * 🐛 Fix SwitcherConfigurationModal check when using localhost
  * 🚚 Move README at the top of the project
  * 📝 Add notes to install chromedriver from binary
  * 🐛 Fix issues with scenic-selenium due to changed selectors
